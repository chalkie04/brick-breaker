#------imports------#
import tkinter
#-------------------#
#-------------------defines the entry class----------------#
class Entry: #creates the class
    def __init__(self): #defines the init of the class
        self.mw = tkinter.Tk() #creates a window using tkinter

        self.label = tkinter.Label(self.mw, text = "Enter Name") #sets the label of the window as "enter name"
        self.entry = tkinter.Entry(self.mw, width = 15) #creates an entry space for the user to enter a name
        self.button = tkinter.Button(self.mw, text = "Done", command = self.get_value) #creates a button, which calls the get_value function on being pressed
        self.label.pack(side="top") #sets the position of the label
        self.entry.pack(side="top") #sets the position of the entry
        self.button.pack(side="top") #sets the position of the button

        tkinter.mainloop() #runs the tkinter mainloop

    def get_value(self): #get_value function, used to retrieve the name typed into the entry
        self.name = self.entry.get() #sets the variable self.name to what was typed into the entry
        self.mw.destroy() #closes the tkinter window
#----------------------------------------------------------#
