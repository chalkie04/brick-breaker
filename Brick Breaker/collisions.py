#------------------imports----------------#
import pygame, ball, player, brick
#-----------------------------------------#
#------------Pre-requisites------------#
screenX = 1300 #variables for screen dimensions
screenY = 820 #

score = 0 #score variable
lives = 3 #lives variable
sounds = [] #array with which sounds will be played
pygame.mixer.init() #initialises the pygame mixer
pygame.mixer.set_num_channels(16) #set the number of audio channels to 16
#--------------------------------------#
#------------------------Player/ball collision detection--------------------------#
def player_collisions(p): #defines the player collision function, requiring a player variable
    testbat = pygame.sprite.spritecollideany(p, ball.ball_list) #uses pygame to return any ball sprites that have collided with the player
    global sounds #allows for the use of the sounds array

    if testbat != None: #tests to see if the collision test has returned anything
        vx = (p.rect.x - testbat.rect.x) #finds the difference in x positions of the player and the collided ball sprite
        vy = (p.rect.y - testbat.rect.y) #finds the difference in y position
        if vx <= 30: #if the value of vx is less than 30 (width of the ball), it means that the ball cannot be colliding with the left hand side of the player
            if vx <= -221 and vy <= 25: #if these value are true, then the ball must be colliding with the right hand side of the player
                testbat.velx = -testbat.velx #flips the x velocity of the ball to bounce the ball off the right hand side of the player
                testbat.rect.x += (testbat.velx + 2) #move the ball slightly to the right to avoid it from constantly colliding with the side of the player
                sounds.append(pygame.mixer.Sound("Assets/sound2.ogg")) #adds a sound to the sound array so that it can be played
            else: #if the previus conditions are not true, then the ball must be colliding with the top or bottom of the player
                testbat.vely = -testbat.vely #flips the y velocity to bounce the ball off the top/bottom of the player
                testbat.rect.y -= 2 #moves the ball slighly to avoid constant colliding
                sounds.append(pygame.mixer.Sound("Assets/sound2.ogg")) #adds sound to be played
                if p.ismovingR == True: #tests to see if the Player is moving right at the time of collision, this is used to add velocity to the ball or reverse the direction if the ball is moving in an opposite direction to the player
                    if testbat.velx >= testbat.base_speed: #tests to see if the ball is moving in the same direction as the player (right)
                        testbat.velx += 6 #adds velocity if true
                    else: #if the ball isnt moving in the same direction, then the direction is reveresed
                        testbat.velx = testbat.base_speed #reverses the direction of the ball
                if p.ismovingL == True: #same as the ismovingR tests, exceppt in this case testing for moving left
                    if testbat.velx <= -testbat.base_speed: #
                        testbat.velx -= 6 #
                    else: #
                        testbat.velx = -testbat.base_speed #
        elif vy <= 25: #if the ball is low enough and is colliding with the left side of the player, then it will bounce off the left hand side
            testbat.velx = -testbat.velx #flips the direction
            testbat.rect.x -= (testbat.velx -2) #moves slightly to avoid constant collisions
            sounds.append(pygame.mixer.Sound("Assets/sound2.ogg")) #adds sound
#---------------------------------------------------------------------------------#
#----------------------------ball/brick collision detection---------------------------#
def ball_collisions(b): #ball collision function, requiring a ball variable
    test = pygame.sprite.spritecollideany(b, brick.brick_list) #the same as the player collisions, except this case test for a collision between the ball and any bricks
    global score #allows for the use of the socre variable

    if test != None: #tests if the test returns a collision
        vx = (b.rect.x - test.rect.x) #finds the x difference
        vy = (b.rect.y - test.rect.y) #finds the y difference

        if vx >= 0: #if the x difference is greater than 0, then the ball cannot be on the left hand side of the brick
            if vx >= 100: #if this is true than the ball must be on the right hand side of the brick
                b.velx = -b.velx #flips the direction of the ball
                score += test.points #adds the brick's value to the score
                test.kill() #kills the brick that collided with the ball
                test = None #setst the test value to None
                sounds.append(pygame.mixer.Sound("Assets/sound3.ogg")) #adds a sound to play
                if b.velx > b.base_speed: #tests to see if the ball is moving faster than its base speed
                    b.velx -= 1 #reduces the speed of the ball when colliding with a brick
            else: #the ball must be colliding with the top or bottom of the brick
                b.vely = -b.vely #flips the direction of the ball
                score += test.points #adds points to score
                test.kill() #kills the brick
                test = None #sets the test to None
                sounds.append(pygame.mixer.Sound("Assets/sound3.ogg")) #adds a sound to play
                if b.velx > b.base_speed: #the same condition as before for reducing the speed of the ball if it is moving faster than its base speed
                    b.velx -= 1 #reduces the speed
        else: #if he first statement is false then the ball must be colliding with the left hand side of the brick
            b.velx = -b.velx #flips the direction
            score += test.points #adds score
            test.kill() #kills the brick
            test = None #sets the test to None
            sounds.append(pygame.mixer.Sound("Assets/sound3.ogg")) #adds a sound to play
            if b.velx > b.base_speed: #condition used to reduce the speed of the ball
                b.velx -= 1 #reduces the speed of the ball
#-------------------------------------------------------------------------------------#
#------------------------window/player/ball collision detection-------------------------------#
def window_collisions(p, b): #window collisions function, requires a ball and player variable
    global lives #allows for the use of the lives variable

    if b.rect.x >= (screenX - 30): #tests if the ball has gone off the right of the screen
        b.velx = -b.velx #flips the direction of the ball
        b.rect.x = (screenX -30) #sets the position of the ball to the edge of the screen
        sounds.append(pygame.mixer.Sound("Assets/sound2.ogg")) #adds a sound to play

    if b.rect.x <= 0: #tests if the ball has gone off the left of the screen
        b.velx = -b.velx #flips direction
        b.rect.x = 0 #sets position to the edge of the screen
        sounds.append(pygame.mixer.Sound("Assets/sound2.ogg")) #adds sound to play

    if b.rect.y >= (screenY): #tests if the ball has gone off the bottom of the screen
        lives -= 1 #reduces the amount of lives
        b.started = False #sets the ball to not started
        sounds.append(pygame.mixer.Sound("Assets/sound4.ogg")) #adds sound to play

    if b.rect.y <= 0: #tests if the ball has gone off the top of the screen
        b.vely = -b.vely #flips the direction
        sounds.append(pygame.mixer.Sound("Assets/sound2.ogg")) #adds sound to play

    if p.rect.x >= (screenX - 221): #tests if the player has gone off the right of the screen
        p.rect.x = (screenX - 221) #stops the player from moving beyond the edge of the screen
        p.ismovingR = False #sets the player as not moving when at the edge of the screen

    if p.rect.x <= 0: #tests if the player has gone off the left of the screen
        p.rect.x = 0 #stops the player moving beyond the edge of the screen
        p.ismovingL = False #sets the player as not moving
#--------------------------------------------------------------------------------------------#
#------------------play sounds funcion-------------------#
def play_sounds(): #play sounds function
    global sounds #allows for the use of the sounds array
    if len(sounds) > 0: #tests if there is anything in the sounds array
        for sound in sounds: #goes through every sound found in the array
            channel = pygame.mixer.find_channel() #finds a channel for the sound to be played in
            if channel != None: #if a channel is found, plays the sound
                channel.play(sound) #plays the sound
                sounds.remove(sound) #removes the sound from the sounds array
#--------------------------------------------------------#
