#----------imports----------#
import pygame, sys
from pygame.locals import *
#---------------------------#
#-------------Pre-requisites-------------#
ball_list = pygame.sprite.Group() #creates the ball sprite group
#----------------------------------------#
#--------------------Defining the ball class----------------------#
class Ball(pygame.sprite.Sprite): #creates the ball class, as a child object of the pygame sprite class
    """Ball"""
    def __init__(self, sx, sy): #defines the init of the class, requiring a start x and a start y position
        super().__init__() #calls the init of the parent class

        self.base_speed = 5 #sets the base speed of the ball, the slowest it will move
        self.velx = 0 #sets the x velocity of the ball
        self.vely = 0 #sets the y velocity of the ball
        self.image = pygame.image.load("Assets/Ball.png").convert() #loads an image to be the sprite
        self.image.set_colorkey((255,255,255)) #sets the transparency colour of the image as being white, so the corners of the loaded image become transparent (because the ball image is a circle)
        self.rect = self.image.get_rect() #sets the rect of the ball as the dimensions of the loaded image
        self.rect.x = sx #the x position of the ball will be the start x variable
        self.rect.y = sy #the y position of the ball will be the start y variable
        self.started = False #creates the started variable

    def update(self): #overwrite the sprite class's defualt update function
        self.rect.move_ip(self.velx, self.vely) #updates the position of the ball by adding the velocity to the current positions

        if self.velx < self.base_speed and self.velx > -self.base_speed: #checks if the x velocity of the ball is slower than its base speed (for transitions between difficulties)
            test = self.base_speed - self.velx #finds the difference between the current velocity and the base speed
            if test < self.base_speed: #if the test produces a positve number, then the ball is moving with a positive velocity
                self.velx = self.base_speed #sets the velocity to the positive base speed
            elif test > self.base_speed: #if the test produces a negative number, then the ball is moving with a negative velocity
                self.velx = -(self.base_speed) #sets the velocity to the negative base speed
        if self.vely < self.base_speed and self.vely > -self.base_speed: #same as the x velocity tests, but instead using the y velocity
            test = self.base_speed - self.vely #
            if test < self.base_speed: #
                self.vely = self.base_speed #
            elif test > self.base_speed: #
                self.vely = -(self.base_speed) #
#------------------------------------------------------------------#
