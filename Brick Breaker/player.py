#---------imports----------#
import pygame, sys
from pygame.locals import *
#--------------------------#
#--------------Pre-requisites---------------#
p_list = pygame.sprite.Group() #creates the player sprite group
#-------------------------------------------#
#--------------------------Defining the Player class------------------------#
class Player(pygame.sprite.Sprite): #creates the Player class as a child class of the pygame Sprite class
    """Player"""
    def __init__(self): #defines the init comditions for the Player class, in this case none
        super().__init__() #calls the init function of the parent class

        self.image = pygame.image.load("Assets/player.png").convert() #sets the  image variable of the player by loading the player image
        self.rect = self.image.get_rect() #sets the rect variable by getting the dimensions of the loaded player image
        self.rect.x = 490 #sets the x position of the player
        self.rect.y = 720 #sets the y position of the player
        self.ismovingL = False #creates a variable which is used to show if the player is moving left
        self.ismovingR = False #creates a variable which is used to show if the player is moving right

    def update(self): #overwrites the defualt update class of the parent class
        self.ismovingL = False #after each frame, these varibales are reset to allow for the varibales to only be True when the appropiate keys are pressed in the main game loop
        self.ismovingR = False #
#---------------------------------------------------------------------------#
