#--------------------Imports-------------------#
import pygame, sys, brick, player, ball, collisions, menu
from pygame.locals import *
#----------------------------------------------#
#----------Creation of game pre-requisites------------------#
all_sprites = pygame.sprite.Group() #creates the group needed for all the sprites to be rendered in the main game Loop
brick.create_layers() #generates the brick sprites and positions for the first game to be played in the game loop

bground = pygame.image.load("Assets/background.png").convert() #loads the main game loop background image
background = pygame.transform.scale(bground, (menu.screenX, menu.screenY)) #scales the background image to fit the game window

p = player.Player() #creates the player paddle to be used in the game
player.p_list.add(p) #adds the player sprite to the player sprite group
b = ball.Ball(490, 600) #creates the ball sprite to be used in the game
ball.ball_list.add(b) #adds the ball sprite to the ball sprite group

score = pygame.font.Font(None, 36) #creates a font object for the score text
lives = pygame.font.Font(None, 36) #creates a font object for the lives text
timer = pygame.font.Font(None, 36) #creates a font object for the timer text
game_difficulty = pygame.font.Font(None, 36) #creates a font object for the difficulty text

all_sprites.add(brick.brick_list, player.p_list, ball.ball_list) #adds the other sprite groups to the main sprite group

seconds = 0.0 #creates the initial seconds variable
minutes = 0 #creates the initial minutes variable
difficulty = 1 #creates the initial difficulty variable
#-----------------------------------------------------------#
#---Running the main menu---#
menu.menu() #calls the menu function, which runs the main menu before the main game
#---------------------------#
#-----------------------------------Main Game Loop---------------------------------------------#
while True: #starts a the main game while loop, which runs indefinately as True will always equal True
    for event in pygame.event.get(): #creates a loop that goes through each event in pygame
        if event.type == QUIT: #tests for if an event in pygame is a QUIT type
            pygame.quit() #if a QUIT event is present, this code will shut down the game
            sys.exit() #also closes the game if a QUIT event is found
        if event.type == pygame.KEYDOWN: #tests for if an event in pygame is a key being pressed
            if b.started == False: #tests for if the ball has started to move independantly, this value will be false at the start of a game or after a life has been lost
                if event.key == K_SPACE: #tests for if an event in pygame is the space bar being pressed, as this is the control for launching the ball
                    b.velx = b.base_speed #sets the ball's velocity to the base speed of the ball at that given time
                    b.vely = -b.base_speed #
                    b.started = True #sets the started variable to True so that the game loop logic will know that the ball has started moving independantly
            if event.key == K_ESCAPE: #tests for if an event in pygame is the escape key being pressed, as this will control the pause menu
                menu.menu_sprites.empty() #emtpys the menu sprites group so that none of the sprites, created in the previous menu drawing, cause any graphical bugs
                menu.menu() #calls the main menu function so the game screen changes to the menu screen

    if b.started == False: #tests if the ball has not started, this is used to make the ball appear above the player when the game first starts or when a life is lost
        b.velx = 0 #sets the ball's velocity to 0, so the ball will not move
        b.vely = 0 #
        b.rect.x = p.rect.x + 95 #sets the ball's positional values so that the ball will appear above the player
        b.rect.y = p.rect.y - 55 #

    if b.started == True: #tests if the ball has started, this is used to update the timer text with the correct amount of time
        seconds += menu.fps_clock.get_time() / 1000 #the time passed since the last call of get_time() (in milliseconds) is added to the seconds variable, to be used to display the game time
        if seconds >= 60.0: #tests if 60 seconds have passed, this is used to increase the minutes value and therefore the amount of minutes displayed on the timer
            minutes += 1 #increases the minutes value by 1
            seconds = 0.0 #resets the seconds value to 0, as one minute has passed
            difficulty += 1 #increases the difficulty value, so the difficulty text will increase
            b.base_speed += 1 #the base speed of the ball is increased, this is what is used to increase the game's difficulty over time

    ks = pygame.key.get_pressed() #get all the keys currently being pressed on the keyboard
    if ks[pygame.K_d] or ks[pygame.K_RIGHT]: #tests if the keys D or RIGHT are being pressed, this os used to control the player movement
        p.rect.x += 12 #increases the players x position, moving it right
        p.ismovingR = True #sets the moving right variable to True so that velocity can be added to the ball if a collision occurs (in collisions.py)
    if ks[pygame.K_a] or ks[pygame.K_LEFT]: #tests if the keys A or LEFT are being pressed, used to control player movement
        p.rect.x += -12 #decreases the players x position, moving it left
        p.ismovingL = True #sets the moving left variable to True, for use with adding velocity

    collisions.window_collisions(p, b) #calls the window_collisions function to test for window collisions
    collisions.player_collisions(p) #calls the player_collisions function to test for player/ball collisions
    collisions.ball_collisions(b) #calls the ball_collisions function to test for ball/brick collisions
    collisions.play_sounds() #plays the sounds resulting from collisions found by the other collision functions

    score_text = score.render("Score: " + str(collisions.score), 1, (255,255,255)) #renders the score text
    lives_text = lives.render("Lives: " + str(collisions.lives), 1, (255,255,255)) #renders the lives text
    timer_text = timer.render("Time: " + str(minutes) + ":" + str(int(seconds)), 1, (255,255,255)) #renders the timer text
    difficulty_text = game_difficulty.render("Difficulty: " + str(difficulty), 1, (255,255,255)) #renders the difficulty text

    if len(brick.brick_list) == 0: #tests to see if the brick sprite group is empty, this is used to regenerate more bricks to continue playing
        brick.create_layers() #generates the new bricks
        all_sprites.add(brick.brick_list) #adds the new bricks to the brick sprite group
        p.rect.x = 490 #sets the player position to the middle of the screen
        b.started = False #sets the ball to not started

    if collisions.lives == 0: #tests to see if the player has run out of lives, used to bring up the end screen
        menu.end_menu() #brings up the end screen
        collisions.lives = 3 #resets the amount of lives to 3
        collisions.score = 0 #resets the score to 0
        difficulty = 1 #resets the difficulty to 1
        b.base_speed = 5 #resets the base speed of the ball to 5
        minutes = 0 #resets the minutes to 0
        seconds = 0.0 #resets the seconds to 0
        all_sprites.empty() #emptys the main sprite group, so the remaining bricks from the previous game are removed
        brick.brick_list.empty() #emptys the bricks sprite group to remove the remaining bricks from this group as well
        brick.create_layers() #gnerates a new set of bricks for the next game
        all_sprites.add(brick.brick_list, player.p_list, ball.ball_list) #adds all the player, ball and brick sprites back to the main group
        p.rect.x = 490 #sets the position of the player to the middle of the screen
        b.started = False #sets the ball to not started, as the previous game has finished

    all_sprites.draw(menu.window) #draws all the sprites in the main group to the screen
    all_sprites.update() #calls the update function on all sprites in this group
    pygame.display.update() #updates the screen
    menu.window.blit(background,(0,0)) #draws the background onto the screen
    menu.window.blit(score_text,(0,0)) #draws the score text
    menu.window.blit(lives_text, (1200,0)) #draws the lives text
    menu.window.blit(timer_text, (400,0)) #draws rhe timer text
    menu.window.blit(difficulty_text, (700,0)) #draws the difficulty text
    menu.fps_clock.tick(60) #sets the frame rate of the game to 60 fps
#---------------------------------------------------------------------------------------------#
