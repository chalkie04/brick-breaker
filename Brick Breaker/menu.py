#-------------------imports-------------------#
import pygame, os, sys, collisions, entry, pickle
from pygame.locals import *
#---------------------------------------------#
#--------------------------------Pre-requisites------------------------------------#
os.environ['SDL_VIDEO_CENTERED'] = '1' #makes the pygame window appear in the center of the monitor/screen

pygame.init() #initialises pygame
screenX = 1300 #screen x dimension
screenY = 820 #screen y dimension

fps_clock = pygame.time.Clock() #creates a pygame clock
window = pygame.display.set_mode((screenX, screenY), HWSURFACE | DOUBLEBUF) #creates a pygame window using the screen x/y
pygame.display.set_caption("Brick Breaker 0.1.4") #sets the caption of the pygame window
menu_sprites = pygame.sprite.Group() #creates the main menu sprites group

menubackground = pygame.image.load("Assets/menubackground.png").convert() #loads the main menu background image
background = pygame.image.load("Assets/background.png").convert() #loads the primary background for the other menus
optionsbackground = pygame.image.load("Assets/optionsbackground.png").convert() #loads the options secondary background
endbackground = pygame.image.load("Assets/endbackground.png").convert() #loads the end secondary background

firstplay = True #sets the first play variable to True, used to bring up the help screen on the first game to be played

try: #tries to load the high scores file, if the file does not exist; then a new one will be created
    open_file = open("scores.dat", "rb") #loads the file
    scores = pickle.load(open_file) #loads the contents of the file
    open_file.close() #closes the file
except IOError: #if the file does not exist
    scores = [["test", "0"]] #creates a test score
    open_file = open("scores.dat", "wb") #creates the file
    pickle.dump(scores, open_file) #write the score to the file
    open_file.close() #closes the file
#----------------------------------------------------------------------------------#
#--------------------------defines the menu object class-------------------------------#
class MenuObject(pygame.sprite.Sprite): #defines the MenuObject class as a child class of the pygame sprite class
    """MenuObject"""
    def __init__(self, button, selectedimage=None): #defines the init of the class requiring a button image and an optional selected image
        super().__init__() #calls the init of the parent class
        self.image2 = pygame.image.load(button).convert() #loads the image provided by the button variable
        self.image = self.image2 #creates a second image varibale so that the sprite can be switched between the non-selected and the selected buttons
        self.rect = self.image.get_rect() #creates the rect variable using the dimensions of the loaded image
        self.selected = False #sets selected variable, used to determine if the MenuObject is selected

        if selectedimage == None: #if the optional selectedimage is not used
            self.selectedimage = self.image #sets the selectedimage to be the same as the non-selected image
        else: #if a selectedimage is present
            self.selectedimage = pygame.image.load(selectedimage).convert() #the selectedimage is set tot the image loaded using the selectedimage variable

    def update(self): #overwrites the defualt sprite update
        if self.selected == True: #if the image is selected
            self.image = self.selectedimage #the current visible sprite becomes the selectedimage
        else: #if the MenuObject is not selected
            self.image = self. image2 #then the non-selected image is visible
#--------------------------------------------------------------------------------------#
#--------------------------------------main menu function----------------------------------------#
def menu(): #main menu function
    global firstplay #allows use of the firstplay variable
    running = True #running varibale to control the menu loop
    play = MenuObject("Assets/play.png", "Assets/playselected.png") #creates a MenuObject called play
    options = MenuObject("Assets/options.png", "Assets/optionsselected.png") #creates a MenuObject called options
    exit = MenuObject("Assets/exit.png", "Assets/exitselected.png") #creates a MenuObject called exit
    menu_sprites.add(play, options, exit) #adds the MenuObject sprites to the main group
    play.rect.x = 523 #sets the x/y positions of the MenuObjects
    play.rect.y = 200 #
    options.rect.x = 523 #
    options.rect.y = 400 #
    exit.rect.x = 523 #
    exit.rect.y = 600 #
    selectednum = 0 #used to control which MenuObject is selected

    while running == True: #main menu loop
        for event in pygame.event.get(): #tests for events in pygame
            if event.type == QUIT: #if a QUIT event is present
                pygame.quit() #closes the game
                sys.exit() #also closes thew game
            if event.type == pygame.KEYDOWN: #if a key pressed event is present
                if event.key == pygame.K_UP: #if the key pressed id UP
                    selectednum -= 1 #the selectednum is changed to select the previous MenuObject
                    collisions.sounds.append(pygame.mixer.Sound("Assets/sound1.ogg")) #adds a sound to be played
                if event.key == pygame.K_DOWN: #if the key pressed is DOWN
                    selectednum += 1 #the selectednum is changed to select the next MenuObject
                    collisions.sounds.append(pygame.mixer.Sound("Assets/sound1.ogg")) #adds a sound to be played
                if event.key == pygame.K_RETURN: #if the key pressed is RETURN
                    if play.selected == True: #if the play MenuObject is selected
                        running = False #stops the main loop
                        menu_sprites.empty() #emptys the sprite group to prevent graphical errors
                        if firstplay == True: #if this is true, then the game about to be played is the first
                            firstplay = False #sets firstplay to false
                            help_menu() #lauches the help screen
                    if options.selected == True: #if the optioins MenuObject is selected
                        running = False #stops the main loop
                        menu_sprites.empty() #emptys the main sprite group to prevent graphical errors
                        options_menu() #lauches the options menu
                    if exit.selected == True: #if the exit MenuObject is selected
                        pygame.quit() #closes the game
                        sys.exit() #closes the game

        if selectednum == 0: #if the selectednum = 0, then play will be the selected MenuObject
            play.selected = True #
            options.selected = False #
            exit.selected = False #
        elif selectednum == 1: #if the selectednum = 1, then options will be the selected MenuObject
            options.selected = True #
            play.selected = False #
            exit.selected = False #
        elif selectednum == 2: #if the selectednum = 2, then exit will be the selected MenuObject
            exit.selected = True #
            play.selected = False #
            options.selected = False #
        elif selectednum > 2: #if the selectedunum goes over 2, it will be reset to 0
            selectednum = 0 #
        elif selectednum < 0: #if the selectednum goes below 0, it will jump to 2
            selectednum = 2 #

        menu_sprites.draw(window) #draws the menu sprites to the screen
        menu_sprites.update() #calls the update function on all the sprites
        collisions.play_sounds() #plays sounds

        pygame.display.update() #updates the screen
        window.blit(menubackground, (0,0)) #draws the menu background to the screen
        fps_clock.tick(60) #sets the framerate to 60 fps
#-----------------------------------------------------------------------------------------------#
#------------------------------options menu function------------------------------------#
def options_menu(): #options menu function
    running = True #the same loop and events handling as the main menu loop
    back = MenuObject("Assets/backselected.png") #creates a MenuObject called back
    back.rect.x = 523 #sets the x/y position
    back.rect.y = 600 #
    back.selected = True #back will always be selected as it is the only MenuObject in this menu
    menu_sprites.add(back) #adds back to the main sprite group
    while running == True: #same main loop as the main menu
        for event in pygame.event.get(): #
            if event.type == QUIT: #
                pygame.quit() #
                sys.exit() #
            if event.type == pygame.KEYDOWN: #
                if event.key == pygame.K_RETURN: #
                    running = False #
                    menu_sprites.empty() #

        menu_sprites.draw(window) #draws/updtaes sprites and updates the screen
        menu_sprites.update() #

        pygame.display.update() #
        window.blit(background, (0,0)) #draws the menu's primary background
        window.blit(optionsbackground, (0,0)) #draws the menu's secondary background
        draw_scores(scores, window) #draws the high scores to the options menu
        fps_clock.tick(60) #sets fps to 60
    menu() #calls the main menu if this menu is exited
#---------------------------------------------------------------------------------------#
#--------------------------------help menu function--------------------------------#
def help_menu(): #help menu function
    running = True #the same loop and events handling as the main menu
    play = MenuObject("Assets/playselected.png") #creates a MenuObject called play
    play.rect.x = 523 #sets the x/y positions
    play.rect.y = 600 #
    play.selected = True #as with the options menu, there is only one MenuObject, thus it will always be selected
    menu_sprites.add(play) #adds the play sprites to the main group
    while running == True: #the same loop as the main menu
        for event in pygame.event.get(): #
            if event.type == QUIT: #
                pygame.quit() #
                sys.exit() #
            if event.type == pygame.KEYDOWN: #
                if event.key == pygame.K_RETURN: #
                    running = False #
                    menu_sprites.empty() #

        menu_sprites.draw(window) #draws/updtaes sprites and updates the display
        menu_sprites.update() #

        pygame.display.update() #
        window.blit(background, (0,0)) #draws the primary background
        window.blit(optionsbackground, (0,0)) #draws the secondary background
        draw_scores(scores, window) #draws the high scores to the menu
        fps_clock.tick(60) #sets the fps to 60
#----------------------------------------------------------------------------------#
#--------------------------------------end menu function------------------------------------------#
def end_menu(): #end menu function
    running = True #same loop and events handling as the other menus
    menu_button = MenuObject("Assets/menu.png","Assets/menuselected.png") #creates a MenuObject called menu button
    save = MenuObject("Assets/save.png","Assets/saveselected.png") #creates a MenuObject called save
    menu_button.rect.x = 523 #sets the x/y positions
    menu_button.rect.y = 600 #
    save.rect.x = 523 #
    save.rect.y = 400 #
    menu_sprites.add(menu_button, save) #adds menu and save to the main sprite group
    selectednum = 0 #
    end_score = pygame.font.Font(None, 70) #creates the end score font object
    score_text = end_score.render(str(collisions.score), 1, (255,255,255)) #renders the end score text

    while running == True: #same loop as the main menu
        for event in pygame.event.get(): #
            if event.type == QUIT: #
                pygame.quit() #
                sys.exit() #
            if event.type == pygame.KEYDOWN: #
                if event.key == pygame.K_UP: #
                    selectednum -= 1 #
                    collisions.sounds.append(pygame.mixer.Sound("Assets/sound1.ogg")) #
                if event.key == pygame.K_DOWN: #
                    selectednum += 1 #
                    collisions.sounds.append(pygame.mixer.Sound("Assets/sound1.ogg")) #
                if event.key == pygame.K_RETURN: #
                    if save.selected == True: #if save is selected
                        entry_box = entry.Entry() #creates an entry object to take input from the user
                        try: #tries to tale the input from the entry object
                            name = entry_box.name #
                        except: #if it fails, the name "Name" will be used
                            name = "Name" #
                        if name == "": #if nothing is input by the user in the text box, then "Name" will also be used
                            name = "Name" #
                        high_scores = open("scores.dat", "rb") #opens the high scores file
                        test = pickle.load(high_scores) #loads the saved scores from the file
                        if len(test) == 1: #if there is only one high score, then it will be the "test 0" generated when making a new high score file
                            high_scores.close() #closes the file
                            open_file = open("scores.dat", "wb") #opens the file in write mode
                            scores.insert(0,[name, str(collisions.score)]) #adds the games score to the list
                            pickle.dump(scores, open_file) #writes the scores list to the file
                            open_file.close() #closes the file
                        else: #if there is mulptiple high scores
                            for i in range(len(test)): #for each high score
                                if int(test[i][1]) <= collisions.score: #if the high score is less than the end score, then the end score will be inserted in that index
                                    high_scores.close() #closes the file
                                    open_file = open("scores.dat", "wb") #open the file in write mode
                                    scores.insert(i, [name, str(collisions.score)]) #inserts the end score into the score list
                                    pickle.dump(scores, open_file) #writes the updated score list to the file
                                    open_file.close() #closes the file
                                    break #ends the loop
                        running = False #closes the menu loop
                        menu_sprites.empty() #emptys the main sprite group to prevent issues

                    if menu_button.selected == True: #menu buttom returns to the main menu
                        running = False #
                        menu_sprites.empty() #

        if selectednum == 0: #same selecting logic as the main menu
            save.selected = True #
            menu_button.selected = False #
        elif selectednum == 1: #
            save.selected = False #
            menu_button.selected = True #
        elif selectednum > 1: #
            selectednum = 0 #
        elif selectednum < 0: #
            selectednum = 1 #

        menu_sprites.draw(window) #draws/updtaes the sprites and updates the display
        menu_sprites.update() #
        collisions.play_sounds() #

        pygame.display.update() #
        window.blit(background, (0,0)) #draws the primary background
        window.blit(endbackground, (0,0)) #draws the secondary background
        window.blit(score_text, (700, 135)) #draws the end score text
        fps_clock.tick(60) #sets the fps to 60
    menu() #calls the main menu after the loop stops
#------------------------------------------------------------------------------------------------------#
#-------------------------draw scores function------------------------------#
def draw_scores(scores, screen): #draw scores function, requires score list and screen
    for i in range(len(scores)): #for every score in the score list
        if i < 7: #limits the number of high scores displayed to 7
            name = pygame.font.Font(None, 36) #creates the name font
            score = pygame.font.Font(None, 36) #creates the score font
            screen.blit(name.render(scores[i][0], 1, (255,255,255)), (860, (60*(i+1))+30)) #draws the name text
            screen.blit(score.render(scores[i][1], 1, (255,255,255)), (1150,(60*(i+1))+30)) #draws the score text
#---------------------------------------------------------------------------#
