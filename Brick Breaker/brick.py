#-----------imports-----------#
import pygame, sys, random
from pygame.locals import *
#-----------------------------#
#-----------Pre-requisites-----------#
brick_list = pygame.sprite.Group() #creates the sprite group that will contain the brick sprites
#------------------------------------#
#-------------------------Defining the Brick class---------------------------#
class Brick(pygame.sprite.Sprite): #creates the brick class as a child class of the pygame Sprite class
    """Brick"""
    def __init__(self, colour): #defines the init conditions for the brick class, requiring a colour variable which determines the colour of the brick created
        super().__init__() #calls the init function of the parent class

        self.image = pygame.image.load("Assets/brick"+str(colour)+".png").convert() #sets the image variable of the brick by loading an image using the colour variable to determine which image is loaded
        self.rect = self.image.get_rect() #sets the rect variable of the brick by getting the dimensions of the loaded image
        self.points = 10 #sets the points variable of the brick, which determines the amount of score the brick is worth when it is hit by the ball
        if colour == 6: #tests for if the colour selected is gold (brick6.png)
            self.points = 20 #if the brick is gold, the amount of score the brick is worth is 20
#----------------------------------------------------------------------------#
#----------------------------Defining the create_layers function---------------------------#
def create_layers(): #defines the name and requisites for the function (in this case none)
    offset = 50 #sets the offset variable, which determines the size of the border around the bricks seperating them from the edges of the game window
    y = 36 #sets the y variable (the height of one brick), this is used to space apart each brick by the same height as a brick, meaning the bricks appear to be positioned next to each other
    c = 1 #sets the c variable, which is used as the colour variable when creating the bricks
    for i in range(10): #creates a loop that loops 10 times, allowing for 10 rows to be created
        x = 100 #sets the x variable (the width of one brick), this is used to space each brick by the same width as a brick
        for j in range(12): #creates a loop that loops 12 times, allowing for 12 bricks to be created, making one row
            if random.randrange(1,20) == 1: #uses a random number generator to create a value and tests for if the value is 1, this is used to create a 1 in 20 chance for any brick to be gold
                b = Brick(6) #creates a gold brick
                b.rect.x = x*j + offset #sets the x position of the gold brick by using the x, j and offset variables created
                b.rect.y = y*i + offset #sets the y position of the gold brick by using the y, i and offset variables created
                brick_list.add(b) #adds the gold brick to the brick sprite group
            else: #if the generator makes a value that is not 1, then it will make a brick using the c variable as the colour
                b = Brick(c) #creates a brick using c as the colour
                b.rect.x = x*j + offset #sets the x position using x, j and offset
                b.rect.y = y*i + offset #sets the y position using y, i and offset
                brick_list.add(b) #adds the brick to the brick sprite group
        c+=1 #increases the c variable so that the colour of each row changes
        if c >= 6: #tests to see if the c variable has increased to 6, which would make it gold
            c = 1 #c is set back to 1 to prevent the next brick from being gold unintentially
#------------------------------------------------------------------------------------------#
